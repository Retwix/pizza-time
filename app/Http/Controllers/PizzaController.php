<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pizza;
use App\Models\Ingredient;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class PizzaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pizza = pizza::all();

        return response()->json($pizza);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rulesPizza = array(
          'name' => 'required',
          'price' => 'required',
        );
        $validator = Validator::make($request->payload, $rulesPizza);
        if ($validator->fails()) {
          return response()->json(['status' => '400', 'error' => $validator]);
        } else {
          $pizza = new Pizza;
          $pizza->name = $request->payload["name"];
          $pizza->price = $request->payload["price"];
          $pizza->save();
          $this->saveIngredient($request, $pizza->id);

          return response()->json(['status' => '200', 'flash' => 'Pizza créée avec succès!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if(Pizza::find($id)) {
        $pizza = Pizza::find($id);
        $ingredients = Ingredient::where('pizza_id', $id)->get();
        return response()->json([ 'pizza' => Pizza::find($id), 'ingredients' => $ingredients, 'status' => '200']);
      } else {
        return response()->json(['status' => '404']);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function saveIngredient(Request $request, $pizzaId)
    {
      foreach ($request->payload['ingredients'] as $ingredient) {
        $newIngredient = new Ingredient;
        $newIngredient->pizza_id = $pizzaId;
        $newIngredient->name = $ingredient["value"];
        $newIngredient->poids = $ingredient["poids"];
        $newIngredient->save();
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->payload);
        $rulesPizza = array(
          'name' => 'required',
          'price' => 'required',
        );
        $validator = Validator::make($request->payload, $rulesPizza);
        if ($validator->fails()) {
          return response()->json(['status' => '400', 'error' => $validator]);
        } else {
          $pizza = Pizza::find($id);
          $pizza->name = $request->payload["name"];
          $pizza->price = $request->payload["price"];
          $pizza->save();

          $pizza->ingredients()->delete();
          $this->saveIngredient($request, $pizza->id);
          return response()->json(['status' => '200', 'flash' => 'Pizza modifié avec succès!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $pizza = Pizza::find($id);
        $ingredients = Ingredient::where('pizza_id', $id);
        $pizza->delete();
        $ingredients->delete();
        return response()->json(['status' => '200', 'flash'=> 'Pizza supprimé avec succès']);
    }
}
