<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/pizza', 'App\Http\Controllers\PizzaController@index');
Route::get('/pizza/{id}', 'App\Http\Controllers\PizzaController@show');
Route::post('/pizza/store', 'App\Http\Controllers\PizzaController@store');
Route::delete('/pizza/destroy/{id}', 'App\Http\Controllers\PizzaController@destroy');
Route::put('/pizza/edit/{id}', 'App\Http\Controllers\PizzaController@update');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
