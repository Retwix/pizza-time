<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Pizza-time</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    </head>
    <body class="antialiased">
      <div id="app">
        <div class="container">
          <div class="col mt-4">
            <router-view></router-view>
          </div>
        </div>
      </div>

      <script src="{{ asset('js/app.js') }}" charset="utf-8"></script>
    </body>
</html>
